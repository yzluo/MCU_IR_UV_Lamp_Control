#define READ_IR        R62                                //定义红外接收管脚

unsigned char        ir_step;                                //解码步
unsigned char        ir_code_cnt;                        //解码计时
unsigned char        ir_table;                                //解码值

bit         ir_status;                                        //状态
bit         ir_pin_old;                                        //管脚
bit         ir_pin_now;                                        //管脚
bit         ir_bit_ok;                                        //接收到1 BIT

void Ir_Code(void)
{
        if(ir_code_cnt<255)
                ir_code_cnt ++;                        //脉宽计时
        ir_pin_now = READ_IR;                        //读取IO状态

        if((ir_pin_now) && (ir_pin_now != ir_pin_old))                //捕捉脉冲上升沿
        {
                if(ir_code_cnt < 8)                                                        //脉冲1.125ms, -> 0
                {
                        ir_bit_ok = 1;                                                        //读取到1 bit
                        ir_pin_old = 0;                                                        //此处为借用变量
                }
                else if(ir_code_cnt < 16)                                        //脉冲2.25ms, -> 1
                {
                        ir_bit_ok = 1;
                        ir_pin_old = 1;        
                }          
                if(ir_code_cnt > 56)                                                //脉冲13.5ms, -> 包头
                {                                                                                //计时时间一定要适当放宽
                        ir_step=0;                                                        //解码步复位
                }
                ir_code_cnt = 0;                                                        //计时复位
        }
        if(ir_bit_ok)                                                                        //有读取到内容
        {
                ir_bit_ok = 0;                                                                //先复位
                if(ir_step < 24)                                                        //只接收24bit 
                {
                        ir_table >>= 1;                                                //移位
                        if(ir_pin_old)        ir_table+= 0x80;
                }

                if(ir_step == 7)                                                        //第一个字节数据
                {
                        if(ir_table !=0x80)                ir_step = 0;                 //如果用户码为0x80,允许继续
                }
                if(ir_step == 15)
                {
                        if(ir_table !=0x7f)                ir_step = 0;                 //如果用户反码为0x7f,允许继续
                }
                if(ir_step == 23)                                                        //好了,接收到按键值了
                {
                        ir_status = 1;                                                        //置位状态,标识可读取
                }
                if(ir_step < 255)                                                        //接收步计数
                        ir_step ++;
        }
        
        ir_pin_old = ir_pin_now;                                                //管脚状态
}

void main()
{
        while(1)
        {
                if(ir_status)                                                //如果有接收标识
                {
                        ir_status = 0;                                        //复位标识
                        switch(ir_table)                                //键值使用
                        {
                                case 0x01:
                                        ......
                                break;
                        }
                }
        }
}

//0.2ms中断
void _intcall interrupt(void) @ int 
{
        Ir_Code();                                                                        //中断里调用
}

